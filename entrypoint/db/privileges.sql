
create DATABASE IF NOT EXISTS `vehicle` COLLATE 'utf8_general_ci' ;
grant all on `pim`.* TO 'dev'@'%' ;

grant all privileges  on *.* to 'root'@'%' ;

CREATE USER webdev IDENTIFIED BY '123456' ;
grant all privileges  on *.* to 'webdev'@'%' ;

FLUSH PRIVILEGES ;
